
import cirq
import cirq_google
import matplotlib.pyplot as plotlib

# Create four qubits
q = cirq.LineQubit.range(4)

# Apply Hadamard to each qubit
circuit = cirq.Circuit([cirq.H.on_each(*q), cirq.measure(*q)])

# Simulate 1000 measurements
result = cirq.Simulator().run(circuit, repetitions=100)

# Plot a histogram
cirq.vis.plot_state_histogram(result, plotlib.subplot())

plotlib.show()
