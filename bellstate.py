
import cirq
import cirq_google

# Create two qubits
q0, q1 = cirq.LineQubit.range(2)

# Create the circuit to generate a Bell state: 1/sqrt(2) * ( |00⟩ + |11⟩ )

print('Build the circuit:')
bell_circuit = cirq.Circuit()
bell_circuit.append(cirq.H(q0))
bell_circuit.append(cirq.CNOT(q0,q1))

# Print the circuit

print(bell_circuit)
print()

print('Unitary operator corresponding to the circuit')
print(cirq.unitary(bell_circuit))
print()

# Initialize Simulator
simulator = cirq.Simulator()

print('Simulate the circuit:')
results = simulator.simulate(bell_circuit)
print(results)
print()

# For sampling, we need to add a measurement at the end
bell_circuit.append(cirq.measure(q0, q1, key='result'))

print('Sample the circuit:')
samples = simulator.run(bell_circuit, repetitions=1000)
# Print a histogram of results
print(samples.histogram(key='result'))


