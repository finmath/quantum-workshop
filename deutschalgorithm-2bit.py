
import cirq
import cirq_google

# Define the qubits to use. We have n+1 qubits. n for the function arguments, 1 for the function value.
q0, q1, q2 = cirq.LineQubit.range(3)

# The oracle function maps |x>|y> to |x>|y + f(x)>. This is, for any f, an invertible unitary operator function.

# Define the two possible constant functions
constant = (
    [],             # always 0
    [cirq.X(q2)]    # always 1
)
# Define the six possible balanced functions
balanced = (
    [cirq.CNOT(q0, q2)],
    [cirq.CNOT(q1, q2)],
    [cirq.CNOT(q0, q2), cirq.CNOT(q1, q2)],
    [cirq.CNOT(q0, q2), cirq.X(q2)],
    [cirq.CNOT(q1, q2), cirq.X(q2)],
    [cirq.CNOT(q0, q2), cirq.CNOT(q1, q2), cirq.X(q2)]
)

"""Creating the circuit used in Deutsch's algorithm."""
def deutsch_algorithm(oracle):
    """Returns the circuit for Deutsch's algorithm given an input
    oracle, i.e., a sequence of operations to query a particular function.
    """
    # Apply H to q0,q1 (Deutsch algortihm)
    yield cirq.H(q0), cirq.H(q1)

    # Convert the function value f(x) to a phase (-1)^{f(x)) in qubit q2
    yield cirq.X(q2)        # move q2 to |1>
    yield cirq.H(q2)        # move q2 to |-> = |0> - |1>
    yield oracle            # apply (x,q2) -> (x,q2+f(x)) which creates the phase factor (-1)^{f(x))
    yield cirq.H(q2)        # moved q2 from |-> to |1>
    yield cirq.X(q2)        # move q2 from |1> to  |0>

    # Apply H to q0,q1 (Deutsch algortihm)
    yield cirq.H(q0), cirq.H(q1)

    yield cirq.measure(q0), cirq.measure(q1), cirq.measure(q2)

"""Simulate each of the circuits."""
simulator = cirq.Simulator()

print("Constant functions:")
for oracle in constant:
    print(f"Circuit for f:")
    print(cirq.Circuit(deutsch_algorithm(oracle)), end="\n\n")
    result = simulator.run(cirq.Circuit(deutsch_algorithm(oracle)), repetitions=10)
    print('oracle: results:\n{}'.format(result))
    print()

print()
print("Balanced functions:")
for oracle in balanced:
    print(f"Circuit for f:")
    print(cirq.Circuit(deutsch_algorithm(oracle)), end="\n\n")
    result = simulator.run(cirq.Circuit(deutsch_algorithm(oracle)), repetitions=10)
    print('oracle: results:\n{}'.format(result))
    print()
