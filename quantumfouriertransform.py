
import cirq
import numpy as np
import matplotlib.pyplot as plt

# Create the circuit for qft on a list of qubits
def qft_circuit_gates(q : list):
    """Returns the circuit for the QFT  """
    numberOfQubits = len(q)
    for j in range(numberOfQubits):
        # Apply Hadamardt gate
        yield cirq.H(q[j])
        for k in range(j + 1, numberOfQubits):
            # Apply conditional phase shift
            yield cirq.CZ(q[j], q[k]) ** (pow(2, -(k - j)))
    # Finally reverse bit order
    for j in range(int(numberOfQubits / 2)):
        # Apply swap gates
        yield cirq.SWAP(q[j], q[numberOfQubits - 1 - j])

# Create qubits
numberOfQubits = 4
q = cirq.LineQubit.range(numberOfQubits)

print('Build the circuit:')
qft_circuit = cirq.Circuit(qft_circuit_gates(q))

qft_operation = cirq.qft(*q, without_reverse=False)
qft_circuit_build_in = cirq.Circuit(qft_operation)

# Print the circuit
print(qft_circuit)
print()

print('Unitary operator corresponding to the circuit')
print(cirq.unitary(qft_circuit))
print()

# Initialize Simulator
simulator = cirq.Simulator()

# works only for 4 qubits since vector is hard coded to 2^^ = 16
print('Simulate the circuit: constant')
results = simulator.simulate(qft_circuit, initial_state=np.array([1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.])/np.sqrt(16))
print(np.array2string(np.round(results.final_state_vector,3)).replace('\n', ''))
print()


print('Simulate the circuit: wave')
results = simulator.simulate(qft_circuit, initial_state=np.array([1., -1., 1., -1., 1., -1., 1., -1., 1., -1., 1., -1., 1., -1., 1., -1.])/np.sqrt(16))
print(np.array2string(np.round(results.final_state_vector,3)).replace('\n', ''))
print()

print('Simulate the circuit: other wave')
results = simulator.simulate(qft_circuit, initial_state=np.array([+1., 0., -1., 0, +1., 0., -1., 0, +1., 0., -1., 0, +1., 0., -1., 0.])/np.sqrt(8))
print(np.array2string(np.round(results.final_state_vector,3)).replace('\n', ''))
print()

print('Simulate the circuit: peak (will create a wave)')
results1 = simulator.simulate(qft_circuit, initial_state=np.array([0., 1., 0., 0., 0., 0., 0.0, 0., 0., 0., 0., 0, 0., 0., 0.0, 0.])/np.sqrt(1))
print(np.array2string(np.round(results1.final_state_vector,3)).replace('\n', ''))
print()

N = 16
x_f = np.linspace(0.0, 1.0, N)
plt.plot(x_f, np.real(np.round(results1.final_state_vector,3)))
plt.show()
