# quantum-workshop: A Short Introduction to Quantum Computing

## Introduction

This project contains some experiments related to
*Quamtum Computing*.

It is part of the mini-lecture A Short Introduction to Quantum Computing

The experiments are implemented in Python/Cirq

## Cirq Experiments

1. `bellstate.py` - create the Bell state and investigate it.
1. `deutschalgorithm-1bit.py` - very simple version of the Deutsch-Josza algorithm for a function mapping 1 bit to 1 bit.
1. `deutschalgorithm-2bit.py` - very simple version of the Deutsch-Josza algorithm for a function mapping 2 bits to 1 bit.
1. `quantumfouriertransform.py` - an implementation of a quantum fourier transform for n bits (with test cases for n=4)
1. `simulator.py` - Perform a simulation and plot a histogram.

### Bell State

Build a circuit that creates the Bell state - a maximal entangled state in 2-qubit quantum register: ( |00> + |11> ) / sqrt(2) - when applyed to |00>.

